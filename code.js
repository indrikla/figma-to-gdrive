if (figma.command === "run") {
    figma.showUI(__uiFiles__.main);
    figma.ui.resize(400, 300);
    figma.ui.postMessage("reset");
    figma.ui.onmessage = response => {
        if (figma.currentPage.selection.length > 0) {
            if (response.type === 'uploadButt') {
                var figmaFileURL = response.figmaLink;
                var figmaFileKey = response.figmaKey;
                var fileName = [];
                var fileNodeID = [];
                var obj = {
                    "fileURL": figmaFileURL,
                    "fileKey": figmaFileKey,
                    table: []
                };
                for (let i = 0; i < figma.currentPage.selection.length; i++) {
                    fileName[i] = figma.currentPage.selection[i].name + ".png";
                    var tempNodeArr = figma.currentPage.selection[i].id.split(":");
                    fileNodeID[i] = tempNodeArr[0] + "%3A" + tempNodeArr[1];
                    obj.table.push({
                        "name": fileName[i],
                        "fileID": figma.currentPage.selection[i].id,
                        "fileNodeID": fileNodeID[i]
                    });
                }
                figma.ui.postMessage(obj);
                if (response.status === 'OK') {
                    figma.notify('SUKSES KEREN LO ANJjjjjj!');
                }
                else {
                    figma.closePlugin(response.status);
                }
                //            figma.ui.postMessage("reset")
            }
        }
        else {
            figma.notify("Please select one or more object(s) to export");
        }
    };
}
if (figma.command === "setAPIKey") {
    figma.clientStorage.getAsync('figmaAPIKey').then(apiKey => {
        figma.showUI(__uiFiles__.setAPIKey, { visible: true });
        figma.ui.postMessage({
            type: 'getKey',
            apikey: apiKey
        });
        figma.ui.onmessage = response => {
            figma.clientStorage.setAsync('figmaAPIKey', response.apiKey).then(() => {
                if (response.success === true) {
                    figma.closePlugin('Succeed!');
                }
            });
        };
    });
}
